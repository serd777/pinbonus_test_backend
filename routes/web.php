<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'backend'], function() {
	Route::get('/', function() { return view('index'); });

	Route::group(['prefix' => 'auth'], function() {
		Route::get('vk', 'VKApi\AuthController@auth')->middleware('guest');
		Route::get('logout', 'VKApi\AuthController@logout')->middleware('auth');
	});

	Route::group(['prefix' => 'user'], function() {
		Route::get('info', 'UserController@getUser')->middleware('rest_auth');
	});

	Route::group(['prefix' => 'adv'], function() {
		Route::get('get_cabinets', 'VKApi\AdvertismentController@getCabinets')->middleware('rest_auth');
	});
});