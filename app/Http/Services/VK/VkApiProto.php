<?php namespace App\Http\Services\VK;

/**
 * Прототип методов вызова ВК
 * @package App\Http\Services\VK
 */
class VkApiProto {
	/**
	 * @var string URL до ВК
	 */
	protected $url;

	/**
	 * VkApi constructor.
	 *
	 * @param string $method_name
	 * @param string $token
	 */
	public function __construct($method_name, $token) {
		$this->url = "https://api.vk.com/method/{$method_name}?v=" . config('services.vk_api.version') .
		             '&access_token=' . $token;
	}

	/**
	 * Метод, отсылающий данные VK и возвращающий ответ
	 * @return object
	 */
	protected function exec() {
		return json_decode(file_get_contents($this->url));
	}
}