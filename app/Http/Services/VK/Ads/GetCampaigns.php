<?php namespace App\Http\Services\VK\Ads;

use App\Http\Services\VK\VkApiProto;

/**
 * Возвращает рекламные компании
 * @package App\Http\Services\VK\Ads
 */
class GetCampaigns extends VkApiProto {
	/**
	 * Конструктор
	 *
	 * @param string $token
	 * @param int $cabinet_id
	 */
	public function __construct($token, $cabinet_id) {
		parent::__construct('ads.getCampaigns', $token);

		//  Запишем параметры метода
		$this->url .= "&account_id=" . $cabinet_id;
	}

	/**
	 * Метод, возвращающий список компаний
	 * @return mixed
	 */
	public function get() {
		return $this->exec()->response;
	}
}