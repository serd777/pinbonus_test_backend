<?php namespace App\Http\Services\VK\Ads;

use App\Http\Services\VK\VkApiProto;

/**
 * Возвращает рекламные объявления
 * @package App\Http\Services\VK\Ads
 */
class GetAds extends VkApiProto {
	/**
	 * Конструктор
	 *
	 * @param string $token
	 * @param int $cabinet_id
	 * @param int $company_id
	 */
	public function __construct($token, $cabinet_id, $company_id) {
		parent::__construct('ads.getAds', $token);

		//  Запишем параметры метода
		$this->url .= "&account_id=" . $cabinet_id;
		$this->url .= "&campaign_ids=" . json_encode([$company_id]);
	}

	/**
	 * Метод, возвращающий список кабинетов
	 * @return mixed
	 */
	public function get() {
		return $this->exec()->response;
	}
}