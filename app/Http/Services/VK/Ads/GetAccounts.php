<?php namespace App\Http\Services\VK\Ads;

use App\Http\Services\VK\VkApiProto;

/**
 * Возвращает список кабинетов
 * @package App\Http\Services\VK\Ads
 */
class GetAccounts extends VkApiProto {
	/**
	 * Конструктор
	 *
	 * @param string $token
	 */
	public function __construct($token) {
		parent::__construct('ads.getAccounts', $token);
	}

	/**
	 * Метод, возвращающий список кабинетов
	 * @return mixed
	 */
	public function get() {
		return $this->exec()->response;
	}
}