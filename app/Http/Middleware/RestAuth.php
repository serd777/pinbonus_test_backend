<?php

namespace App\Http\Middleware;

use App\Http\ServerAnswer;
use Closure;

class RestAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	if(!\Auth::check())
    		return ServerAnswer::failed();

        return $next($request);
    }
}
