<?php namespace App\Http;

/**
 * Класс, содержащий методы ответов клиентам
 * @package App\Http\Controllers
 */
class ServerAnswer
{
	/**
	 * Успешный ответ
	 * @param null|mixed $data
	 * @return \Illuminate\Http\JsonResponse
	 */
	public static function success($data = null) {
		return \Response::json([
			'status' => true,
			'data'   => $data,
		]);
	}
	/**
	 * Неуспешный ответ
	 * @param null|mixed $data
	 * @return \Illuminate\Http\JsonResponse
	 */
	public static function failed($data = null) {
		return \Response::json([
			'status' => false,
			'data'   => $data,
		]);
	}
}