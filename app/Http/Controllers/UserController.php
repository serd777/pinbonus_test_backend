<?php namespace App\Http\Controllers;

use App\Http\Models\UserPublic;
use App\Http\ServerAnswer;

/**
 * Контроллер обработки данных пользователя
 * @package App\Http\Controllers
 */
class UserController extends Controller {
	/**
	 * Возвращает модель пользователя
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getUser() {
		return ServerAnswer::success(
			UserPublic::query()
			          ->find( \Auth::user()->id )
		);
	}
}