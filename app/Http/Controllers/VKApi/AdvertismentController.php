<?php namespace App\Http\Controllers\VKApi;

use App\Http\Controllers\Controller;
use App\Http\Models\VkCabinet;
use App\Http\Models\VkCampaign;
use App\Http\ServerAnswer;
use App\Http\Services\VK\Ads;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

/**
 * Контроллер управления модулем advertisment
 * @package App\Http\Controllers\VKApi
 */
class AdvertismentController extends Controller {
	/**
	 * Метод, возвращающий список кабинетов
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function getCabinets() {
		if(\Cache::has('cabinets_user_' . \Auth::user()->id))
			return \Cache::get('cabinets_user_' . \Auth::user()->id);

		$data = collect((new Ads\GetAccounts(\Auth::user()->access_token))->get());
		$cabinets = $this->parseCabinets($data);

		//  Кэшируем на 15 минут
		\Cache::put('cabinets_user_' . \Auth::user()->id, $cabinets, 15);

		return ServerAnswer::success($cabinets);
	}

	/**
	 * Метод, возвращающий список компаний
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getCampaigns(Request $request) {
		$validation = \Validator::make($request->all(), [
			'remote_id' => 'required|numeric'
		]);
		if($validation->fails())
			return ServerAnswer::failed($validation->errors());

		if(\Cache::has('campaigns_user_' . \Auth::user()->id . '_' . $request->input('remote_id') ))
			return \Cache::get('campaigns_user_' . \Auth::user()->id . '_' . $request->input('remote_id'));

		$data = collect( (new Ads\GetCampaigns(\Auth::user()->access_token, $request->input('remote_id')))->get() );
		$campaigns = $this->parseCampaigns($data, $request->input('remote_id'));

		//  Кэшируем на 15 минут
		\Cache::put('campaigns_user_' . \Auth::user()->id . '_' . $request->input('remote_id'), $campaigns, 15);

		return ServerAnswer::success($campaigns);
	}

	/**
	 * Преобразует полученные данные от вк в СУБД
	 *
	 * @param Collection $data
	 *
	 * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
	 */
	private function parseCabinets($data) {
		//  Получим те кабинеты, которые уже лежат в базе
		$remote_ids = $data->pluck('account_id');
		$db_list = VkCabinet::query()->whereIn('remote_id', $remote_ids)->get();

		//  А теперь найдем те которые не в базе и добавим их
		foreach($data as $item) {
			if(!$db_list->where('remote_id', $item->account_id)->first())
				$db_list->push(
					VkCabinet::query()->create([
						'user_id'       => \Auth::user()->id,
						'remote_id'     => $item->account_id,
						'remote_type'   => $item->account_type,
						'remote_status' => $item->account_status,
						'remote_role'   => $item->access_role
					])
				);
		}

		return $db_list;
	}

	/**
	 * Преобразует полученные данные от вк в СУБД
	 * @param $data
	 * @param $company_remote_id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	private function parseCampaigns($data, $company_remote_id) {
		$company = VkCabinet::query()
		                    ->where('remote_id', $company_remote_id)
		                    ->first();

		//  Получим те компании, которые уже лежат в базе
		$remote_ids = $data->pluck('remote_id');
		$db_list = VkCampaign::query()->whereIn('remote_id', $remote_ids)->get();

		//  А теперь найдем те которые не в базе и добавим их
		foreach($data as $item) {
			if(!$db_list->where('remote_id', $item->id)->first())
				$db_list->push(
					VkCampaign::query()->create([
						'cabinet_id'        => $company->id,
						'remote_id'         => $item->id,
						'remote_type'       => $item->type,
						'remote_name'       => $item->name,
						'remote_status'     => $item->status,
						'remote_day_limit'  => $item->day_limit,
						'remote_all_limit'  => $item->all_limit,
						'remote_start_time' => $item->start_time,
						'remote_stop_time'  => $item->stop_time
					])
				);
		}

		return $db_list;
	}
}