<?php namespace App\Http\Controllers\VKApi;

use App\Http\Controllers\Controller;
use App\Http\Models\User;

/**
 * Контроллер аутентификации
 * @package App\Http\Controllers\VKApi
 */
class AuthController extends Controller {
	/**
	 * @var \OAuth
	 */
	private $oauth;

	/**
	 * Конструктор
	 */
	public function __construct() {
		$this->oauth = \OAuth::consumer('Vkontakte');
	}

	/**
	 * Метод, авторизующий пользователя
	 */
	public function auth() {
		$code = \Request::get('code');

		if(!$code)
			return $this->externalRedirect();

		return $this->authUser($code);
	}

	/**
	 * Метод, делающий выход пользователя
	 */
	public function logout() {
		\Auth::logout();
		return redirect(config('app.url'));
	}

	/**
	 * Редиректит на авторизацию VK
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	private function externalRedirect() {
		return redirect((string)$this->oauth->getAuthorizationUri());
	}

	/**
	 * Возвращает ссылку на получение данных от пользователя
	 * @param $params
	 *
	 * @return string
	 */
	private function getRequestUrl($params) {
		return '/users.get?user_id=' . $params['user_id'] . '&fields=photo_max,photo_max_orig&v=5.73';
	}

	/**
	 * Метод, проводящий физическую авторизацию
	 * @param $code
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	private function authUser($code) {
		//  Получаем access_token
		$token = $this->oauth->requestAccessToken($code);
		//  Дополнительные параметры
		$params = $token->getExtraParams();

		//Отправляем запрос на получение данных о юзере
		$request = $this->oauth->request($this->getRequestUrl($params));
		//Разбираем ответ
		$answer = json_decode($request, true);
		$user_info = $answer['response'][0];

		$user = User::ofVk($user_info['id'])->first();
		if(!$user)
			$user = $this->createUser($user_info);
		else if($this->needUpdate($user, $user_info))
			$this->updateUser($user, $user_info);

		$user->access_token = $token->getAccessToken();
		$user->save();

		\Auth::login($user);
		return redirect(config('app.url') . '/cabinets');
	}

	/**
	 * Проверка на обновление данных пользователя
	 * @param $user
	 * @param $data
	 *
	 * @return bool
	 */
	private function needUpdate($user, $data) {
		return $user->name != ($data['first_name'] . ' ' . $data['last_name']) ||
		       $user->avatar != $data['photo_max_orig'];
	}

	/**
	 * Метод, обновляющий данные пользователя в БД
	 * @param $user
	 * @param $data
	 */
	private function updateUser(&$user, $data) {
		$user->name   = $data['first_name'] . ' ' . $data['last_name'];
		$user->avatar = $data['photo_max_orig'];
	}

	/**
	 * Метод, создающий пользователя в БД
	 * @param $data
	 *
	 * @return User
	 */
	private function createUser($data) {
		$user = User::create([
			'vk_id'  => $data['id'],
			'name'   => $data['first_name'] . ' ' . $data['last_name'],
			'avatar' => $data['photo_max_orig']
		]);

		return $user;
	}
}