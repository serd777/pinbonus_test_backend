<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * VK кабинет рекламной компании
 * @package App\Http\Models
 */
class VkCabinet extends Model {
	/**
	 * Поля таблицы
	 * @var array
	 */
	protected $fillable = [
		'user_id',
		'remote_id', 'remote_type', 'remote_status', 'remote_role'
	];

	/**
	 * Преобразования к типам в ответе
	 * @var array
	 */
	protected $casts = [
		'remote_status' => 'boolean'
	];

	/**
	 * Название таблицы
	 * @var string
	 */
	protected $table = 'vk_cabinets';
}