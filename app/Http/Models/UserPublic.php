<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Публичная модель пользователя (содержит те поля, которые не нужно скрывать)
 * @package App\Http\Models
 */
class UserPublic extends Model {
	protected $visible = [
		'name', 'avatar',
	];
	protected $table = 'users';
}