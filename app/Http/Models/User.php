<?php namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'avatar', 'access_token', 'vk_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

	/**
	 * Поиск по VK ID
	 * @param $query
	 * @param $vk_id
	 *
	 * @return mixed
	 */
    public function scopeOfVk($query, $vk_id) {
    	return $query->where('vk_id', $vk_id);
    }
}
