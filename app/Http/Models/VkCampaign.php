<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Модель ВК компании по рекламе
 * @package App\Http\Models
 */
class VkCampaign extends Model {
	protected $fillable = [
		'cabinet_id',
		'remote_id',
		'remote_type',
		'remote_name',
		'remote_status',
		'remote_day_limit',
		'remote_all_limit',
		'remote_start_time',
		'remote_stop_time',
	];
	protected $table = 'vk_campaigns';
}