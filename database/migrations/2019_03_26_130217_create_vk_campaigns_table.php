<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVkCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vk_campaigns', function (Blueprint $table) {
            $table->bigIncrements('id');

	        $table->unsignedBigInteger('cabinet_id');
	        $table->foreign('cabinet_id')->references('id')->on('vk_cabinets')->onDelete('cascade');

	        $table->unsignedBigInteger('remote_id');
	        $table->string('remote_type', 30);
	        $table->string('remote_name', 128);
	        $table->unsignedSmallInteger('remote_status');

	        $table->unsignedInteger('remote_day_limit');
	        $table->unsignedInteger('remote_all_limit');
	        $table->timestamp('remote_start_time')->nullable();
	        $table->timestamp('remote_stop_time')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vk_campaigns');
    }
}
