<?php

return [

	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => '\\OAuth\\Common\\Storage\\Session',

	/**
	 * Consumers
	 */
	'consumers' => [
		'Vkontakte' => [
			'client_id'     => env('VK_CLIENT_ID', ''),
			'client_secret' => env('VK_SECRET', ''),
			'scope'         => ['photos', 'ads'],
		],
	]

];